#include <string>
#include <cstring>
#include <vector>
#include <iostream>

#include "webgl.h"
#include <node.h>
#include <node_buffer.h>

#ifdef _WIN32
  #define  strcasestr(s, t) strstr(strupr(s), strupr(t))
#endif

using namespace node;
using namespace v8;
using namespace std;

WebGLRenderingContext* WebGLRenderingContext::ACTIVE = NULL;
WebGLRenderingContext* WebGLRenderingContext::CONTEXT_LIST_HEAD = NULL;

//v8::Handle<v8::Value> ThrowError(const char* msg) {
//  return Nan::ThrowError(Nan::New<String>(msg));
//}

#define GL_METHOD(method_name) NAN_METHOD(WebGLRenderingContext:: method_name)

#define GL_BOILERPLATE  \
  Nan::HandleScope();\
  if(info.This()->InternalFieldCount() <= 0) { \
    return Nan::ThrowError("Invalid WebGL Object"); \
  } \
  WebGLRenderingContext* inst = node::ObjectWrap::Unwrap<WebGLRenderingContext>(info.This()); \
  if(!(inst && inst->setActive())) { \
    return Nan::ThrowError("Invalid GL context"); \
  }

// A 32-bit and 64-bit compatible way of converting a pointer to a GLuint.
//static GLuint ToGLuint(const void* ptr) {
//  return static_cast<GLuint>(reinterpret_cast<size_t>(ptr));
//}

//static int SizeOfArrayElementForType(v8::ExternalArrayType type) {
//  switch (type) {
//  case v8::kExternalByteArray:
//  case v8::kExternalUnsignedByteArray:
//    return 1;
//  case v8::kExternalShortArray:
//  case v8::kExternalUnsignedShortArray:
//    return 2;
//  case v8::kExternalIntArray:
//  case v8::kExternalUnsignedIntArray:
//  case v8::kExternalFloatArray:
//    return 4;
//  default:
//    return 0;
//  }
//}

// inline void *getImageData(Local<Value> arg) {
//  void *pixels = NULL;
//  if (!arg->IsNull()) {
//    Local<Object> obj = Local<Object>::Cast(arg);
//    if (!obj->IsObject())
//      Nan::ThrowError("Bad texture argument");
//
//    pixels = obj->GetIndexedPropertiesExternalArrayData();
//  }
//  return pixels;
// }

template<typename Type>
inline Type* getArrayData(Local<Value> arg, int* num = NULL) {
  Type *data=NULL;
  if(num) *num=0;

  if(!arg->IsNull()) {
    if(arg->IsArray()) {
      Nan::ThrowError("Not support array type");
      /*
      Local<Array> arr = Local<Array>::Cast(arg);
      if(num) *num=arr->Length();
      data = reinterpret_cast<Type*>(arr->GetIndexedPropertiesExternalArrayData());*/
    }
    else if(arg->IsObject()) {
      Local<ArrayBufferView> arr = Local<ArrayBufferView>::Cast(arg);
      if(num) *num=arr->ByteLength()/sizeof(Type);
      data = reinterpret_cast<Type*>(arr->Buffer()->GetContents().Data());
    }
    else
      Nan::ThrowError("Bad array argument");
  }

  return data;
}

WebGLRenderingContext::WebGLRenderingContext(
  int width,
  int height,
  int depthSize,
  int msaaSamples) :

  state(GLCONTEXT_STATE_INIT),
  next(NULL),
  prev(NULL) {

  #ifdef USE_AGL
    //Create AGL context
    GLint pixelAttr[] = {
      AGL_RGBA,
      AGL_DOUBLEBUFFER, GL_FALSE,
      AGL_MULTISAMPLE,
      AGL_PIXEL_SIZE, 32,
      AGL_DEPTH_SIZE, depthSize,
      AGL_SAMPLE_BUFFERS_ARB, 1,
      AGL_SAMPLES_ARB, msaaSamples,
      AGL_ACCELERATED,
      AGL_NONE
    };

    AGLPixelFormat aglPixelFormat = aglChoosePixelFormat(NULL, 0, pixelAttr);
    if (aglPixelFormat == NULL) {
      fprintf(stderr, "Error pixel format\n");
      state = GLCONTEXT_STATE_ERROR;
      return;
    }

    context = aglCreateContext(aglPixelFormat, NULL);
    aglDestroyPixelFormat(aglPixelFormat);
    if (context == NULL) {
      fprintf(stderr, "Error creating GL context!\n");
      state = GLCONTEXT_STATE_ERROR;
      return;
    }

    if (!aglSetCurrentContext(context)) {
      fprintf(stderr, "aglSetCurrentContext failed\n");
      state = GLCONTEXT_STATE_ERROR;
      return;
    }
  #endif

  #ifdef USE_GLX

    display = XOpenDisplay(0);

    static int attributeList[] = {
          GLX_RGBA,
          GLX_DOUBLEBUFFER,
          GLX_DEPTH_SIZE, depthSize,
          GLX_RED_SIZE, 1,
          GLX_GREEN_SIZE, 1,
          GLX_BLUE_SIZE, 1,
          GLX_SAMPLES_ARB, msaaSamples,
          GLX_SAMPLE_BUFFERS_ARB, 1,
          None
        };

    XVisualInfo *vi = glXChooseVisual(display, DefaultScreen(display),attributeList);

    //oldstyle context:
    context = glXCreateContext(display, vi, 0, GL_TRUE);

    pixmap = XCreatePixmap (display, DefaultRootWindow(display), width, height, 24);
    glXPixmap = glXCreateGLXPixmap(display, vi, pixmap);

    if (!glXMakeCurrent(display, glXPixmap, context)) {
      fprintf(stderr, "Failed to initialize GLX\n");
      state = GLCONTEXT_STATE_ERROR;
      return;
    }

    // Initialize GLEW
    if (glewInit() != GLEW_OK) {
      fprintf(stderr, "Failed to initialize GLEW\n");
      state = GLCONTEXT_STATE_ERROR;
      return;
    }
  #endif

  //Success
  state = GLCONTEXT_STATE_OK;
  registerContext();
  ACTIVE = this;
}

bool WebGLRenderingContext::setActive() {
  if(state != GLCONTEXT_STATE_OK) {
    return false;
  }
  if(this == ACTIVE) {
    return true;
  }
  bool result = false;
  #ifdef USE_AGL
    result = aglSetCurrentContext(context);
  #endif
  #ifdef USE_GLX
    //result = true;
	result = glXMakeCurrent(display, glXPixmap, context);
  #endif
  if(result) {
    ACTIVE = this;
  }
  return result;
}

void WebGLRenderingContext::setError(GLenum error) {
  if (error == GL_NO_ERROR || lastError != GL_NO_ERROR) {
    return;
  }
  GLenum prevError = glGetError();
  if (prevError == GL_NO_ERROR) {
    lastError = error;
  }
}

void WebGLRenderingContext::dispose() {
  //Unregister context
  unregisterContext();

  if(!setActive()) {
    return;
  }

  //Update state
  state = GLCONTEXT_STATE_DESTROY;

  //Destroy all object references
  //  compactGLObj();
  for(size_t i=0; i<objects.size(); ++i) {
    GLuint obj = objects[i].first;
    switch(objects[i].second) {
      case GLOBJECT_TYPE_PROGRAM:
        glDeleteProgram(obj);
        break;
      case GLOBJECT_TYPE_BUFFER:
        glDeleteBuffers(1,&obj);
        break;
      case GLOBJECT_TYPE_FRAMEBUFFER:
        glDeleteFramebuffers(1,&obj);
        break;
      case GLOBJECT_TYPE_RENDERBUFFER:
        glDeleteRenderbuffers(1,&obj);
        break;
      case GLOBJECT_TYPE_SHADER:
        glDeleteShader(obj);
        break;
      case GLOBJECT_TYPE_TEXTURE:
        glDeleteTextures(1,&obj);
        break;
      default:
        break;
    }
  }
  objects.clear();

  //Destroy context
  #ifdef USE_AGL
    aglDestroyContext(context);
  #endif

  #ifdef USE_GLX
    glXDestroyContext(display, context);
    glXDestroyPixmap (display, glXPixmap);
    XFreePixmap (display, pixmap);
  #endif
}

WebGLRenderingContext::~WebGLRenderingContext() {
  dispose();
}

void WebGLRenderingContext::disposeAll() {
  while(CONTEXT_LIST_HEAD) {
    CONTEXT_LIST_HEAD->dispose();
  }
}

GL_METHOD(New) {
  Nan::HandleScope();

  WebGLRenderingContext* instance = new WebGLRenderingContext(
      info[0]->Int32Value()   //Width
    , info[1]->Int32Value()   //Height
    , info[2]->Int32Value()   //Depth Size
    , info[3]->Int32Value()   //MSAA Samples
//    args[2]->BooleanValue(), //Alpha
//    args[3]->BooleanValue(), //Depth
//    args[4]->BooleanValue(), //Stencil
//    args[5]->BooleanValue(), //antialias
//    args[6]->BooleanValue(), //premultipliedAlpha
//    args[7]->BooleanValue(), //preserve drawing buffer
//    args[8]->BooleanValue(), //low power
//    args[9]->BooleanValue()  //fail if crap
  );
  if(instance->state != GLCONTEXT_STATE_OK){
    return Nan::ThrowError("Error creating WebGLContext");
  }

  instance->Wrap(info.This());
  info.GetReturnValue().Set(info.This());
}

GL_METHOD(Destroy) {
  GL_BOILERPLATE
  inst->dispose();
//  NanReturnUndefined();
}

GL_METHOD(Uniform1f) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  float x = (float) info[1]->NumberValue();

  glUniform1f(location, x);
//  NanReturnUndefined();
}

GL_METHOD(Uniform2f) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  float x = (float) info[1]->NumberValue();
  float y = (float) info[2]->NumberValue();

  glUniform2f(location, x, y);
//  NanReturnValue;
}

GL_METHOD(Uniform3f) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  float x = (float) info[1]->NumberValue();
  float y = (float) info[2]->NumberValue();
  float z = (float) info[3]->NumberValue();

  glUniform3f(location, x, y, z);
//  NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform4f) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  float x = (float) info[1]->NumberValue();
  float y = (float) info[2]->NumberValue();
  float z = (float) info[3]->NumberValue();
  float w = (float) info[4]->NumberValue();

  glUniform4f(location, x, y, z, w);
//  NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform1i) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int x = info[1]->Int32Value();

  glUniform1i(location, x);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform2i) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int x = info[1]->Int32Value();
  int y = info[2]->Int32Value();

  glUniform2i(location, x, y);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform3i) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int x = info[1]->Int32Value();
  int y = info[2]->Int32Value();
  int z = info[3]->Int32Value();

  glUniform3i(location, x, y, z);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform4i) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int x = info[1]->Int32Value();
  int y = info[2]->Int32Value();
  int z = info[3]->Int32Value();
  int w = info[4]->Int32Value();

  glUniform4i(location, x, y, z, w);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform1fv) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int num=0;
  GLfloat *ptr=getArrayData<GLfloat>(info[1],&num);
  glUniform1fv(location, num, ptr);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform2fv) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int num=0;
  GLfloat *ptr=getArrayData<GLfloat>(info[1],&num);
  num /= 2;

  glUniform2fv(location, num, ptr);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform3fv) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int num=0;
  GLfloat *ptr=getArrayData<GLfloat>(info[1],&num);
  num /= 3;

  glUniform3fv(location, num, ptr);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform4fv) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int num=0;
  GLfloat *ptr=getArrayData<GLfloat>(info[1],&num);
  num /= 4;

  glUniform4fv(location, num, ptr);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform1iv) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int num=0;
  GLint *ptr=getArrayData<GLint>(info[1],&num);

  glUniform1iv(location, num, ptr);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform2iv) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int num=0;
  GLint *ptr=getArrayData<GLint>(info[1],&num);
  num /= 2;

  glUniform2iv(location, num, ptr);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform3iv) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int num=0;
  GLint *ptr=getArrayData<GLint>(info[1],&num);
  num /= 3;
  glUniform3iv(location, num, ptr);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Uniform4iv) {
  GL_BOILERPLATE;

  int location = info[0]->Int32Value();
  int num=0;
  GLint *ptr=getArrayData<GLint>(info[1],&num);
  num /= 4;
  glUniform4iv(location, num, ptr);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(PixelStorei) {
  GL_BOILERPLATE;

  int pname = info[0]->Int32Value();
  int param = info[1]->Int32Value();

  glPixelStorei(pname,param);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(BindAttribLocation) {
  GL_BOILERPLATE;

  int program = info[0]->Int32Value();
  int index = info[1]->Int32Value();
  String::Utf8Value name(info[2]);

  glBindAttribLocation(program, index, *name);

  //NanReturnValue(NanUndefined());
}

GLenum WebGLRenderingContext::getError() {
  GLenum error = glGetError();
  if (lastError != GL_NO_ERROR) {
    error = lastError;
  }
  lastError = GL_NO_ERROR;
  return error;
}

GL_METHOD(GetError) {
  GL_BOILERPLATE;

  info.GetReturnValue().Set(Nan::New<v8::Integer>(inst->getError()));
}


GL_METHOD(DrawArrays) {
  GL_BOILERPLATE;

  int mode = info[0]->Int32Value();
  int first = info[1]->Int32Value();
  int count = info[2]->Int32Value();

  glDrawArrays(mode, first, count);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(UniformMatrix2fv) {
  GL_BOILERPLATE;

  GLint location = info[0]->Int32Value();
  GLboolean transpose = info[1]->BooleanValue();

  GLsizei count=0;
  GLfloat* data=getArrayData<GLfloat>(info[2],&count);

  if (count < 4) {
    Nan::ThrowError("Not enough data for UniformMatrix2fv");
  }

  glUniformMatrix2fv(location, count / 4, transpose, data);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(UniformMatrix3fv) {
  GL_BOILERPLATE;

  GLint location = info[0]->Int32Value();
  GLboolean transpose = info[1]->BooleanValue();
  GLsizei count=0;
  GLfloat* data=getArrayData<GLfloat>(info[2],&count);

  if (count < 9) {
    Nan::ThrowError("Not enough data for UniformMatrix3fv");
  }

  glUniformMatrix3fv(location, count / 9, transpose, data);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(UniformMatrix4fv) {
  GL_BOILERPLATE;

  GLint location = info[0]->Int32Value();
  GLboolean transpose = info[1]->BooleanValue();
  GLsizei count=0;
  GLfloat* data=getArrayData<GLfloat>(info[2],&count);

  if (count < 16) {
    Nan::ThrowError("Not enough data for UniformMatrix4fv");
  }

  glUniformMatrix4fv(location, count / 16, transpose, data);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(GenerateMipmap) {
  GL_BOILERPLATE;

  GLint target = info[0]->Int32Value();
  glGenerateMipmap(target);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(GetAttribLocation) {
  GL_BOILERPLATE;

  int program = info[0]->Int32Value();
  String::Utf8Value name(info[1]);

  GLint result = glGetAttribLocation(program, *name);
  info.GetReturnValue().Set(Nan::New<v8::Integer>(result));
}


GL_METHOD(DepthFunc) {
  GL_BOILERPLATE;

  glDepthFunc(info[0]->Int32Value());

  //NanReturnValue(NanUndefined());
}


GL_METHOD(Viewport) {
  GL_BOILERPLATE;

  int x = info[0]->Int32Value();
  int y = info[1]->Int32Value();
  int width = info[2]->Int32Value();
  int height = info[3]->Int32Value();

  glViewport(x, y, width, height);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(CreateShader) {
  GL_BOILERPLATE;

  GLuint shader=glCreateShader(info[0]->Int32Value());
  #ifdef LOGGING
  cout<<"createShader "<<shader<<endl;
  #endif
  inst->registerGLObj(GLOBJECT_TYPE_SHADER, shader);
  info.GetReturnValue().Set(Nan::New<v8::Integer>(shader));
}


GL_METHOD(ShaderSource) {
  GL_BOILERPLATE;

  int id = info[0]->Int32Value();
  String::Utf8Value code(info[1]);

  const char* codes[1];
  codes[0] = *code;
  GLint length=code.length();

  glShaderSource  (id, 1, codes, &length);

  //NanReturnValue(NanUndefined());
}


GL_METHOD(CompileShader) {
  GL_BOILERPLATE;

  glCompileShader(info[0]->Int32Value());

  //NanReturnValue(NanUndefined());
}

GL_METHOD(FrontFace) {
  GL_BOILERPLATE;

  glFrontFace(info[0]->Int32Value());

  //NanReturnValue(NanUndefined());
}


//GL_METHOD(GetShaderParameter) {
//  GL_BOILERPLATE;
//
//  int shader = info[0]->Int32Value();
//  int pname = info[1]->Int32Value();
//  int value = 0;
//  switch (pname) {
//  case GL_DELETE_STATUS:
//  case GL_COMPILE_STATUS:
//    glGetShaderiv(shader, pname, &value);
//    //    NanReturnValue(JS_BOOL(static_cast<bool>(value!=0)));
//    info.GetReturnValue().Set(Nan::New<v8::Boolean>(value));
//  case GL_SHADER_TYPE:
//    glGetShaderiv(shader, pname, &value);
//    //    NanReturnValue(JS_FLOAT(static_cast<unsigned long>(value)));
//    info.GetReturnValue().Set(Nan::New<v8::Number>(value));
//  case GL_INFO_LOG_LENGTH:
//  case GL_SHADER_SOURCE_LENGTH:
//    glGetShaderiv(shader, pname, &value);
//    //    NanReturnValue(JS_FLOAT(static_cast<long>(value)));
//    info.GetReturnValue().Set(Nan::New<v8::Number>(value));
//  default:
//    char *foo = "GetShaderParameter: Invalid Enum";
//    char numstr[21];
//
//    sprintf(numstr, "%i", pname);
//    cout << "Foobar: " + pname;
////    printf("%i", GL_DELETE_STATUS);
////    printf("%i", GL_COMPILE_STATUS);
////    printf("%i", GL_SHADER_TYPE);
////    printf("%i", GL_INFO_LOG_LENGTH);
////    printf("%i", GL_SHADER_SOURCE_LENGTH);
//    Nan::ThrowTypeError("GetShaderParameter: Invalid Enum);
//  }
////  NanReturnUndefined();
//}

GL_METHOD(GetShaderParameter) {
  GL_BOILERPLATE;

  GLint shader = info[0]->Int32Value();
  GLenum pname = info[1]->Int32Value();

  GLint value;
  glGetShaderiv(shader, pname, &value);

  info.GetReturnValue().Set(Nan::New<v8::Integer>(value));
}

GL_METHOD(GetShaderInfoLog) {
  GL_BOILERPLATE;

  GLint id = info[0]->Int32Value();

  GLint infoLogLength;
  glGetShaderiv(id, GL_INFO_LOG_LENGTH, &infoLogLength);

  char* error = new char[infoLogLength+1];
  glGetShaderInfoLog(id, infoLogLength+1, &infoLogLength, error);

  info.GetReturnValue().Set(
    Nan::New<v8::String>(error).ToLocalChecked());

  delete[] error;
}


GL_METHOD(CreateProgram) {
  GL_BOILERPLATE;

  GLuint program=glCreateProgram();
  #ifdef LOGGING
  cout<<"createProgram "<<program<<endl;
  #endif
  inst->registerGLObj(GLOBJECT_TYPE_PROGRAM, program);
  info.GetReturnValue().Set(Nan::New<v8::Integer>(program));
}


GL_METHOD(AttachShader) {
  GL_BOILERPLATE;

  int program = info[0]->Int32Value();
  int shader = info[1]->Int32Value();

  glAttachShader(program, shader);

  //NanReturnValue(NanUndefined());
}


GL_METHOD(LinkProgram) {
  GL_BOILERPLATE;

  glLinkProgram(info[0]->Int32Value());

  //NanReturnValue(NanUndefined());
}


GL_METHOD(GetProgramParameter) {
  GL_BOILERPLATE;

  int program = info[0]->Int32Value();
  int pname = info[1]->Int32Value();

  int value = 0;
  switch (pname) {
  case GL_DELETE_STATUS:
  case GL_LINK_STATUS:
  case GL_VALIDATE_STATUS:
    glGetProgramiv(program, pname, &value);
    //    NanReturnValue(JS_BOOL(static_cast<bool>(value!=0)));
    info.GetReturnValue().Set(Nan::New<v8::Boolean>(value));
  case GL_ATTACHED_SHADERS:
  case GL_ACTIVE_ATTRIBUTES:
  case GL_ACTIVE_UNIFORMS:
    glGetProgramiv(program, pname, &value);
    //    NanReturnValue(JS_FLOAT(static_cast<long>(value)));
    info.GetReturnValue().Set(Nan::New<v8::Integer>(value));
  default:
    Nan::ThrowTypeError("GetProgramParameter: Invalid Enum");
  }
//  NanReturnUndefined();
}


GL_METHOD(GetUniformLocation) {
  GL_BOILERPLATE;

  int program = info[0]->Int32Value();
  Nan::Utf8String name(info[1]);

  info.GetReturnValue().Set(Nan::New<v8::Integer>(glGetUniformLocation(program, *name)));
}


GL_METHOD(ClearColor) {
  GL_BOILERPLATE;

  float red   = (float) info[0]->NumberValue();
  float green = (float) info[1]->NumberValue();
  float blue  = (float) info[2]->NumberValue();
  float alpha = (float) info[3]->NumberValue();

  glClearColor(red, green, blue, alpha);

  //NanReturnValue(NanUndefined());
}


GL_METHOD(ClearDepth) {
  GL_BOILERPLATE;

  float depth = (float) info[0]->NumberValue();

  glClearDepth(depth);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(Disable) {
  GL_BOILERPLATE;

  glDisable(info[0]->Int32Value());
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Enable) {
  GL_BOILERPLATE;

  glEnable(info[0]->Int32Value());
  //NanReturnValue(NanUndefined());
}


GL_METHOD(CreateTexture) {
  GL_BOILERPLATE;

  GLuint texture;
  glGenTextures(1, &texture);
  inst->registerGLObj(GLOBJECT_TYPE_TEXTURE, texture);
  info.GetReturnValue().Set(Nan::New<v8::Integer>(texture));
}

GL_METHOD(BindTexture) {
  GL_BOILERPLATE;

  int target = info[0]->Int32Value();
  int texture = info[1]->IsNull() ? 0 : info[1]->Int32Value();

  glBindTexture(target, texture);
  //NanReturnValue(NanUndefined());
}

unsigned char* WebGLRenderingContext::unpackPixels(
  GLenum type,
  GLenum format,
  GLint width,
  GLint height,
  unsigned char* pixels) {

  //Compute pixel size
  GLint pixelSize = 1;
  if(type == GL_UNSIGNED_BYTE || type == GL_FLOAT) {
    if(type == GL_FLOAT) {
      pixelSize = 4;
    }
    switch(format) {
      case GL_ALPHA:
      case GL_LUMINANCE:
      break;
      case GL_LUMINANCE_ALPHA:
        pixelSize *= 2;
      break;
      case GL_RGB:
        pixelSize *= 3;
      break;
      case GL_RGBA:
        pixelSize *= 4;
      break;
    }
  } else {
    pixelSize = 2;
  }

  //Compute row stride
  GLint rowStride = pixelSize * width;
  if((rowStride % unpack_alignment) != 0) {
    rowStride += unpack_alignment - (rowStride % unpack_alignment);
  }

  GLint imageSize = rowStride * height;
  unsigned char* unpacked = new unsigned char[imageSize];

  if(unpack_flip_y) {
    for(int i=0,j=height-1; j>=0; ++i, --j) {
      memcpy(
          reinterpret_cast<void*>(unpacked + j*rowStride)
        , reinterpret_cast<void*>(pixels   + i*rowStride)
        , width * pixelSize);
    }
  } else {
    memcpy(
        reinterpret_cast<void*>(unpacked)
      , reinterpret_cast<void*>(pixels)
      , imageSize);
  }

  //Premultiply alpha unpacking
  if(unpack_premultiply_alpha &&
     (format == GL_LUMINANCE_ALPHA ||
      format == GL_RGBA)) {

    for(int row=0; row<height; ++row) {
      for(int col=0; col<width; ++col) {
        unsigned char* pixel = unpacked + (row * rowStride) + (col * pixelSize);
        if(format == GL_LUMINANCE_ALPHA) {
          pixel[0] *= pixel[1] / 255.0;
        } else if(type == GL_UNSIGNED_BYTE) {
          float scale = pixel[3] / 255.0;
          pixel[0] *= scale;
          pixel[1] *= scale;
          pixel[2] *= scale;
        } else if(type == GL_UNSIGNED_SHORT_4_4_4_4) {
          int r = pixel[0]&0x0f;
          int g = pixel[0]>>4;
          int b = pixel[1]&0x0f;
          int a = pixel[1]>>4;

          float scale = a / 15.0;
          r *= scale;
          g *= scale;
          b *= scale;

          pixel[0] = r + (g<<4);
          pixel[1] = b + (a<<4);
        } else if(type == GL_UNSIGNED_SHORT_5_5_5_1) {
          if((pixel[0]&1) == 0) {
            pixel[0] = 1; //why does this get set to 1?!?!?!
            pixel[1] = 0;
          }
        }
      }
    }
  }

  return unpacked;
}


GL_METHOD(TexImage2D) {
 GL_BOILERPLATE;

 int target         = info[0]->Int32Value();
 int level          = info[1]->Int32Value();
 int internalformat = info[2]->Int32Value();
 int width          = info[3]->Int32Value();
 int height         = info[4]->Int32Value();
 int border         = info[5]->Int32Value();
 int format         = info[6]->Int32Value();
 int type           = info[7]->Int32Value();
 //void *pixels       = getImageData(args[8]);
 Nan::TypedArrayContents<unsigned char> pixels(info[8]);

 glTexImage2D(
   target,
   level,
   internalformat,
   width,
   height,
   border,
   format,
   type,
   *pixels);

 // NanReturnValue(NanUndefined());
}


// GL_METHOD(TexImage2D) {
//   GL_BOILERPLATE;
//
//   GLenum target         = info[0]->Int32Value();
//   GLint level           = info[1]->Int32Value();
//   GLenum internalformat = info[2]->Int32Value();
//   GLsizei width         = info[3]->Int32Value();
//   GLsizei height        = info[4]->Int32Value();
//   GLint border          = info[5]->Int32Value();
//   GLenum format         = info[6]->Int32Value();
//   GLint type            = info[7]->Int32Value();
//   Nan::TypedArrayContents<unsigned char> pixels(info[8]);
//
//   if(*pixels) {
//     if(inst->unpack_flip_y || inst->unpack_premultiply_alpha) {
//       unsigned char* unpacked = inst->unpackPixels(
//           type
//         , format
//         , width
//         , height
//         , *pixels);
//       glTexImage2D(
//           target
//         , level
//         , internalformat
//         , width
//         , height
//         , border
//         , format
//         , type
//         , unpacked);
//       delete[] unpacked;
//     } else {
//       glTexImage2D(
//           target
//         , level
//         , internalformat
//         , width
//         , height
//         , border
//         , format
//         , type
//         , *pixels);
//     }
//   } else {
//     size_t length = width * height * 4;
//     char* data = new char[length];
//     memset(data, 0, length);
//     glTexImage2D(
//         target
//       , level
//       , internalformat
//       , width
//       , height
//       , border
//       , format
//       , type
//       , data);
//     delete[] data;
//   }
// }


GL_METHOD(TexParameteri) {
  GL_BOILERPLATE;

  int target = info[0]->Int32Value();
  int pname = info[1]->Int32Value();
  int param = info[2]->Int32Value();

  glTexParameteri(target, pname, param);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(TexParameterf) {
  GL_BOILERPLATE;

  int target = info[0]->Int32Value();
  int pname = info[1]->Int32Value();
  float param = (float) info[2]->NumberValue();

  glTexParameterf(target, pname, param);

  //NanReturnValue(NanUndefined());
}


GL_METHOD(Clear) {
  GL_BOILERPLATE;

  glClear(info[0]->Int32Value());

  //NanReturnValue(NanUndefined());
}


GL_METHOD(UseProgram) {
  GL_BOILERPLATE;

  glUseProgram(info[0]->Int32Value());

  //NanReturnValue(NanUndefined());
}

GL_METHOD(CreateBuffer) {
  GL_BOILERPLATE;

  GLuint buffer;
  glGenBuffers(1, &buffer);
  #ifdef LOGGING
  cout<<"createBuffer "<<buffer<<endl;
  #endif
  inst->registerGLObj(GLOBJECT_TYPE_BUFFER, buffer);
  info.GetReturnValue().Set(Nan::New<v8::Integer>(buffer));
}

GL_METHOD(BindBuffer) {
  GL_BOILERPLATE;

  int target = info[0]->Int32Value();
  int buffer = info[1]->Uint32Value();
  glBindBuffer(target,buffer);

  //NanReturnValue(NanUndefined());
}


GL_METHOD(CreateFramebuffer) {
  GL_BOILERPLATE;

  GLuint buffer;
  glGenFramebuffers(1, &buffer);
  inst->registerGLObj(GLOBJECT_TYPE_FRAMEBUFFER, buffer);
  info.GetReturnValue().Set(Nan::New<v8::Integer>(buffer));
}


GL_METHOD(BindFramebuffer) {
  GL_BOILERPLATE;

  int target = info[0]->Int32Value();
  int buffer = info[1]->IsNull() ? 0 : info[1]->Int32Value();

  glBindFramebuffer(target, buffer);

  //NanReturnValue(NanUndefined());
}


GL_METHOD(FramebufferTexture2D) {
  GL_BOILERPLATE;

  int target = info[0]->Int32Value();
  int attachment = info[1]->Int32Value();
  int textarget = info[2]->Int32Value();
  int texture = info[3]->Int32Value();
  int level = info[4]->Int32Value();

  glFramebufferTexture2D(target, attachment, textarget, texture, level);

  //NanReturnValue(NanUndefined());
}


//GL_METHOD(BufferData) {
//  GL_BOILERPLATE;
//
//  int target = args[0]->Int32Value();
//  if(args[1]->IsObject()) {
//    Local<Object> obj = Local<Object>::Cast(args[1]);
//    GLenum usage = args[2]->Int32Value();
//
//    int element_size = SizeOfArrayElementForType(obj->GetIndexedPropertiesExternalArrayDataType());
//    GLsizeiptr size = obj->GetIndexedPropertiesExternalArrayDataLength() * element_size;
//    void* data = obj->GetIndexedPropertiesExternalArrayData();
//    glBufferData(target, size, data, usage);
//  }
//  else if(args[1]->IsNumber()) {
//    GLsizeiptr size = args[1]->Uint32Value();
//    GLenum usage = args[2]->Int32Value();
//    glBufferData(target, size, NULL, usage);
//  }
//  NanReturnValue(NanUndefined());
//}
//
//
//GL_METHOD(BufferSubData) {
//  GL_BOILERPLATE;
//
//  int target = args[0]->Int32Value();
//  int offset = args[1]->Int32Value();
//  Local<Object> obj = Local<Object>::Cast(args[2]);
//
//  int element_size = SizeOfArrayElementForType( obj->GetIndexedPropertiesExternalArrayDataType());
//  int size = obj->GetIndexedPropertiesExternalArrayDataLength() * element_size;
//  void* data = obj->GetIndexedPropertiesExternalArrayData();
//
//  glBufferSubData(target, offset, size, data);
//
//  NanReturnValue(NanUndefined());
//}


GL_METHOD(BufferData) {
  GL_BOILERPLATE;

  GLint target = info[0]->Int32Value();
  GLenum usage = info[2]->Int32Value();

  if(info[1]->IsObject()) {
    Nan::TypedArrayContents<char> array(info[1]);
    glBufferData(target, array.length(), static_cast<void*>(*array), usage);
  } else if(info[1]->IsNumber()) {
    glBufferData(target, info[1]->Int32Value(), NULL, usage);
  }
}


GL_METHOD(BufferSubData) {
  GL_BOILERPLATE;

  GLenum target = info[0]->Int32Value();
  GLint offset  = info[1]->Int32Value();
  Nan::TypedArrayContents<char> array(info[2]);

  glBufferSubData(target, offset, array.length(), *array);
}


GL_METHOD(BlendEquation) {
  GL_BOILERPLATE;

  int mode=info[0]->Int32Value();;

  glBlendEquation(mode);

  //NanReturnValue(NanUndefined());
}


GL_METHOD(BlendFunc) {
  GL_BOILERPLATE;

  int sfactor=info[0]->Int32Value();;
  int dfactor=info[1]->Int32Value();;

  glBlendFunc(sfactor,dfactor);

  //NanReturnValue(NanUndefined());
}


GL_METHOD(EnableVertexAttribArray) {
  GL_BOILERPLATE;

  glEnableVertexAttribArray(info[0]->Int32Value());

  //NanReturnValue(NanUndefined());
}


GL_METHOD(VertexAttribPointer) {
  GL_BOILERPLATE;

  int indx = info[0]->Int32Value();
  int size = info[1]->Int32Value();
  int type = info[2]->Int32Value();
  int normalized = info[3]->BooleanValue();
  int stride = info[4]->Int32Value();
  long offset = info[5]->Int32Value();

  //    printf("VertexAttribPointer %d %d %d %d %d %d\n", indx, size, type, normalized, stride, offset);
  glVertexAttribPointer(indx, size, type, normalized, stride, (const GLvoid *)offset);

  //NanReturnValue(NanUndefined());
}


GL_METHOD(ActiveTexture) {
  GL_BOILERPLATE;

  glActiveTexture(info[0]->Int32Value());
  //NanReturnValue(NanUndefined());
}


GL_METHOD(DrawElements) {
  GL_BOILERPLATE;

  int mode = info[0]->Int32Value();
  int count = info[1]->Int32Value();
  int type = info[2]->Int32Value();
  GLvoid *offset = reinterpret_cast<GLvoid*>(info[3]->Uint32Value());
  glDrawElements(mode, count, type, offset);
  //NanReturnValue(NanUndefined());
}


GL_METHOD(Flush) {
  GL_BOILERPLATE;
  glFlush();
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Finish) {
  GL_BOILERPLATE;
  glFinish();
  //NanReturnValue(NanUndefined());
}

GL_METHOD(VertexAttrib1f) {
  GL_BOILERPLATE;

  GLuint indx = info[0]->Int32Value();
  float x = (float) info[1]->NumberValue();

  glVertexAttrib1f(indx, x);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(VertexAttrib2f) {
  GL_BOILERPLATE;

  GLuint indx = info[0]->Int32Value();
  float x = (float) info[1]->NumberValue();
  float y = (float) info[2]->NumberValue();

  glVertexAttrib2f(indx, x, y);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(VertexAttrib3f) {
  GL_BOILERPLATE;

  GLuint indx = info[0]->Int32Value();
  float x = (float) info[1]->NumberValue();
  float y = (float) info[2]->NumberValue();
  float z = (float) info[3]->NumberValue();

  glVertexAttrib3f(indx, x, y, z);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(VertexAttrib4f) {
  GL_BOILERPLATE;

  GLuint indx = info[0]->Int32Value();
  float x = (float) info[1]->NumberValue();
  float y = (float) info[2]->NumberValue();
  float z = (float) info[3]->NumberValue();
  float w = (float) info[4]->NumberValue();

  glVertexAttrib4f(indx, x, y, z, w);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(VertexAttrib1fv) {
  GL_BOILERPLATE;

  int indx = info[0]->Int32Value();
  GLfloat *data = getArrayData<GLfloat>(info[1]);
  glVertexAttrib1fv(indx, data);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(VertexAttrib2fv) {
  GL_BOILERPLATE;

  int indx = info[0]->Int32Value();
  GLfloat *data = getArrayData<GLfloat>(info[1]);
  glVertexAttrib2fv(indx, data);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(VertexAttrib3fv) {
  GL_BOILERPLATE;

  int indx = info[0]->Int32Value();
  GLfloat *data = getArrayData<GLfloat>(info[1]);
  glVertexAttrib3fv(indx, data);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(VertexAttrib4fv) {
  GL_BOILERPLATE;

  int indx = info[0]->Int32Value();
  GLfloat *data = getArrayData<GLfloat>(info[1]);
  glVertexAttrib4fv(indx, data);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(BlendColor) {
  GL_BOILERPLATE;

  GLclampf r= (float) info[0]->NumberValue();
  GLclampf g= (float) info[1]->NumberValue();
  GLclampf b= (float) info[2]->NumberValue();
  GLclampf a= (float) info[3]->NumberValue();

  glBlendColor(r,g,b,a);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(BlendEquationSeparate) {
  GL_BOILERPLATE;

  GLenum modeRGB= info[0]->Int32Value();
  GLenum modeAlpha= info[1]->Int32Value();

  glBlendEquationSeparate(modeRGB,modeAlpha);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(BlendFuncSeparate) {
  GL_BOILERPLATE;

  GLenum srcRGB= info[0]->Int32Value();
  GLenum dstRGB= info[1]->Int32Value();
  GLenum srcAlpha= info[2]->Int32Value();
  GLenum dstAlpha= info[3]->Int32Value();

  glBlendFuncSeparate(srcRGB,dstRGB,srcAlpha,dstAlpha);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(ClearStencil) {
  GL_BOILERPLATE;

  GLint s = info[0]->Int32Value();

  glClearStencil(s);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(ColorMask) {
  GL_BOILERPLATE;

  GLboolean r = info[0]->BooleanValue();
  GLboolean g = info[1]->BooleanValue();
  GLboolean b = info[2]->BooleanValue();
  GLboolean a = info[3]->BooleanValue();

  glColorMask(r,g,b,a);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(CopyTexImage2D) {
  GL_BOILERPLATE;

  GLenum target = info[0]->Int32Value();
  GLint level = info[1]->Int32Value();
  GLenum internalformat = info[2]->Int32Value();
  GLint x = info[3]->Int32Value();
  GLint y = info[4]->Int32Value();
  GLsizei width = info[5]->Int32Value();
  GLsizei height = info[6]->Int32Value();
  GLint border = info[7]->Int32Value();

  glCopyTexImage2D( target, level, internalformat, x, y, width, height, border);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(CopyTexSubImage2D) {
  GL_BOILERPLATE;

  GLenum target = info[0]->Int32Value();
  GLint level = info[1]->Int32Value();
  GLint xoffset = info[2]->Int32Value();
  GLint yoffset = info[3]->Int32Value();
  GLint x = info[4]->Int32Value();
  GLint y = info[5]->Int32Value();
  GLsizei width = info[6]->Int32Value();
  GLsizei height = info[7]->Int32Value();

  glCopyTexSubImage2D( target, level, xoffset, yoffset, x, y, width, height);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(CullFace) {
  GL_BOILERPLATE;

  GLenum mode = info[0]->Int32Value();

  glCullFace(mode);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(DepthMask) {
  GL_BOILERPLATE;

  GLboolean flag = info[0]->BooleanValue();

  glDepthMask(flag);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(DepthRange) {
  GL_BOILERPLATE;

  GLclampf zNear = (float) info[0]->NumberValue();
  GLclampf zFar = (float) info[1]->NumberValue();

  glDepthRange(zNear, zFar);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(DisableVertexAttribArray) {
  GL_BOILERPLATE;

  GLuint index = info[0]->Int32Value();

  glDisableVertexAttribArray(index);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Hint) {
  GL_BOILERPLATE;

  GLenum target = info[0]->Int32Value();
  GLenum mode = info[1]->Int32Value();

  glHint(target, mode);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(IsEnabled) {
  GL_BOILERPLATE;

  GLenum cap = info[0]->Int32Value();

  bool ret=glIsEnabled(cap)!=0;
  info.GetReturnValue().Set(Nan::New<v8::Boolean>(ret));
}

GL_METHOD(LineWidth) {
  GL_BOILERPLATE;

  GLfloat width = (float) info[0]->NumberValue();

  glLineWidth(width);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(PolygonOffset) {
  GL_BOILERPLATE;

  GLfloat factor = (float) info[0]->NumberValue();
  GLfloat units = (float) info[1]->NumberValue();

  glPolygonOffset(factor, units);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(SampleCoverage) {
  GL_BOILERPLATE;

  GLclampf value = (float) info[0]->NumberValue();
  GLboolean invert = info[1]->BooleanValue();

  glSampleCoverage(value, invert);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(Scissor) {
  GL_BOILERPLATE;

  GLint x = info[0]->Int32Value();
  GLint y = info[1]->Int32Value();
  GLsizei width = info[2]->Int32Value();
  GLsizei height = info[3]->Int32Value();

  glScissor(x, y, width, height);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(StencilFunc) {
  GL_BOILERPLATE;

  GLenum func = info[0]->Int32Value();
  GLint ref = info[1]->Int32Value();
  GLuint mask = info[2]->Int32Value();

  glStencilFunc(func, ref, mask);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(StencilFuncSeparate) {
  GL_BOILERPLATE;

  GLenum face = info[0]->Int32Value();
  GLenum func = info[1]->Int32Value();
  GLint ref = info[2]->Int32Value();
  GLuint mask = info[3]->Int32Value();

  glStencilFuncSeparate(face, func, ref, mask);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(StencilMask) {
  GL_BOILERPLATE;

  GLuint mask = info[0]->Uint32Value();

  glStencilMask(mask);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(StencilMaskSeparate) {
  GL_BOILERPLATE;

  GLenum face = info[0]->Int32Value();
  GLuint mask = info[1]->Uint32Value();

  glStencilMaskSeparate(face, mask);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(StencilOp) {
  GL_BOILERPLATE;

  GLenum fail = info[0]->Int32Value();
  GLenum zfail = info[1]->Int32Value();
  GLenum zpass = info[2]->Int32Value();

  glStencilOp(fail, zfail, zpass);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(StencilOpSeparate) {
  GL_BOILERPLATE;

  GLenum face = info[0]->Int32Value();
  GLenum fail = info[1]->Int32Value();
  GLenum zfail = info[2]->Int32Value();
  GLenum zpass = info[3]->Int32Value();

  glStencilOpSeparate(face, fail, zfail, zpass);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(BindRenderbuffer) {
  GL_BOILERPLATE;

  GLenum target = info[0]->Int32Value();
  GLuint buffer = info[1]->IsNull() ? 0 : info[1]->Int32Value();

  glBindRenderbuffer(target, buffer);

  //NanReturnValue(NanUndefined());
}

GL_METHOD(CreateRenderbuffer) {
  GL_BOILERPLATE;

  GLuint renderbuffers;
  glGenRenderbuffers(1,&renderbuffers);
  #ifdef LOGGING
  cout<<"createRenderBuffer "<<renderbuffers<<endl;
  #endif
  inst->registerGLObj(GLOBJECT_TYPE_RENDERBUFFER, renderbuffers);
  info.GetReturnValue().Set(Nan::New<v8::Integer>(renderbuffers));
}

GL_METHOD(DeleteBuffer) {
  GL_BOILERPLATE;

  GLuint buffer = info[0]->Uint32Value();

  glDeleteBuffers(1,&buffer);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(DeleteFramebuffer) {
  GL_BOILERPLATE;

  GLuint buffer = info[0]->Uint32Value();

  glDeleteFramebuffers(1,&buffer);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(DeleteProgram) {
  GL_BOILERPLATE;

  GLuint program = info[0]->Uint32Value();

  glDeleteProgram(program);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(DeleteRenderbuffer) {
  GL_BOILERPLATE;

  GLuint renderbuffer = info[0]->Uint32Value();

  glDeleteRenderbuffers(1, &renderbuffer);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(DeleteShader) {
  GL_BOILERPLATE;

  GLuint shader = info[0]->Uint32Value();

  glDeleteShader(shader);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(DeleteTexture) {
  GL_BOILERPLATE;

  GLuint texture = info[0]->Uint32Value();

  glDeleteTextures(1,&texture);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(DetachShader) {
  GL_BOILERPLATE;

  GLuint program = info[0]->Uint32Value();
  GLuint shader = info[1]->Uint32Value();

  glDetachShader(program, shader);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(FramebufferRenderbuffer) {
  GL_BOILERPLATE;

  GLenum target = info[0]->Int32Value();
  GLenum attachment = info[1]->Int32Value();
  GLenum renderbuffertarget = info[2]->Int32Value();
  GLuint renderbuffer = info[3]->Uint32Value();

  glFramebufferRenderbuffer(target, attachment, renderbuffertarget, renderbuffer);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(GetVertexAttribOffset) {
  GL_BOILERPLATE;

  GLuint index = info[0]->Uint32Value();
  GLenum pname = info[1]->Int32Value();
  void *ret=NULL;

  glGetVertexAttribPointerv(index, pname, &ret);
  GLuint offset = static_cast<GLuint>(reinterpret_cast<size_t>(ret));
  info.GetReturnValue().Set(Nan::New<v8::Integer>(offset));
}

GL_METHOD(IsBuffer) {
  GL_BOILERPLATE;

  info.GetReturnValue().Set(
      Nan::New<v8::Boolean>(
        glIsBuffer(info[0]->Uint32Value()) != 0));
}

GL_METHOD(IsFramebuffer) {
  GL_BOILERPLATE;

  info.GetReturnValue().Set(
      Nan::New<v8::Boolean>(
        glIsFramebuffer(info[0]->Uint32Value()) != 0));
}

GL_METHOD(IsProgram) {
  GL_BOILERPLATE;

  info.GetReturnValue().Set(
      Nan::New<v8::Boolean>(
        glIsProgram(info[0]->Uint32Value()) != 0));
}

GL_METHOD(IsRenderbuffer) {
  GL_BOILERPLATE;

  info.GetReturnValue().Set(
      Nan::New<v8::Boolean>(
        glIsRenderbuffer(info[0]->Uint32Value()) != 0));
}

GL_METHOD(IsShader) {
  GL_BOILERPLATE;

  info.GetReturnValue().Set(
      Nan::New<v8::Boolean>(
        glIsShader(info[0]->Uint32Value()) != 0));
}

GL_METHOD(IsTexture) {
  GL_BOILERPLATE;

  info.GetReturnValue().Set(
      Nan::New<v8::Boolean>(
        glIsTexture(info[0]->Uint32Value()) != 0));
}

GL_METHOD(RenderbufferStorage) {
  GL_BOILERPLATE;

  GLenum target = info[0]->Int32Value();
  GLenum internalformat = info[1]->Int32Value();
  GLsizei width = info[2]->Uint32Value();
  GLsizei height = info[3]->Uint32Value();

  glRenderbufferStorage(target, internalformat, width, height);
  //NanReturnValue(NanUndefined());
}

GL_METHOD(RenderbufferStorageMultisample) {
  GL_BOILERPLATE

  GLenum target = info[0]->Int32Value();
  GLsizei samples = info[1]->Int32Value();
  GLenum internalformat = info[2]->Int32Value();
  GLsizei width = info[3]->Uint32Value();
  GLsizei height = info[4]->Uint32Value();

  glRenderbufferStorageMultisample(target, samples, internalformat, width, height);
}

GL_METHOD(BlitFramebuffer) {
  GL_BOILERPLATE

  GLint srcX0 = info[0]->Int32Value();
  GLint srcY0 = info[1]->Int32Value();
  GLint srcX1 = info[2]->Int32Value();
  GLint srcY1 = info[3]->Int32Value();
  GLint dstX0 = info[4]->Int32Value();
  GLint dstY0 = info[5]->Int32Value();
  GLint dstX1 = info[6]->Int32Value();
  GLint dstY1 = info[7]->Int32Value();

  GLbitfield mask = info[8]->Uint32Value();
  GLenum filter   = info[9]->Uint32Value();

  glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
}

GL_METHOD(GetShaderSource) {
  GL_BOILERPLATE;

  GLint shader = info[0]->Int32Value();

  GLint len;
  glGetShaderiv(shader, GL_SHADER_SOURCE_LENGTH, &len);

  GLchar *source = new GLchar[len];
  glGetShaderSource(shader, len, NULL, source);
  v8::Local<v8::String> str = Nan::New<v8::String>(source).ToLocalChecked();
  delete[] source;

  info.GetReturnValue().Set(str);
}

GL_METHOD(ValidateProgram) {
  GL_BOILERPLATE;

  glValidateProgram(info[0]->Int32Value());

  //NanReturnValue(NanUndefined());
}

//GL_METHOD(TexSubImage2D) {
//  GL_BOILERPLATE;
//
//  GLenum target = args[0]->Int32Value();
//  GLint level = args[1]->Int32Value();
//  GLint xoffset = args[2]->Int32Value();
//  GLint yoffset = args[3]->Int32Value();
//  GLsizei width = args[4]->Int32Value();
//  GLsizei height = args[5]->Int32Value();
//  GLenum format = args[6]->Int32Value();
//  GLenum type = args[7]->Int32Value();
//  void *pixels=getImageData(args[8]);
//
//  glTexSubImage2D(target, level, xoffset, yoffset, width, height, format, type, pixels);
//
//  NanReturnValue(NanUndefined());
//}

GL_METHOD(TexSubImage2D) {
  GL_BOILERPLATE;

  GLenum target   = info[0]->Int32Value();
  GLint level     = info[1]->Int32Value();
  GLint xoffset   = info[2]->Int32Value();
  GLint yoffset   = info[3]->Int32Value();
  GLsizei width   = info[4]->Int32Value();
  GLsizei height  = info[5]->Int32Value();
  GLenum format   = info[6]->Int32Value();
  GLenum type     = info[7]->Int32Value();
  Nan::TypedArrayContents<unsigned char> pixels(info[8]);

  if(inst->unpack_flip_y ||
     inst->unpack_premultiply_alpha) {
    unsigned char* unpacked = inst->unpackPixels(
        type
      , format
      , width
      , height
      , *pixels);
    glTexSubImage2D(
        target
      , level
      , xoffset
      , yoffset
      , width
      , height
      , format
      , type
      , unpacked);
    delete[] unpacked;
  } else {
    glTexSubImage2D(
        target
      , level
      , xoffset
      , yoffset
      , width
      , height
      , format
      , type
      , *pixels);
  }
}


//GL_METHOD(ReadPixels) {
//  GL_BOILERPLATE;
//
//  GLint x        = args[0]->Int32Value();
//  GLint y        = args[1]->Int32Value();
//  GLsizei width  = args[2]->Int32Value();
//  GLsizei height = args[3]->Int32Value();
//  GLenum format  = args[4]->Int32Value();
//  GLenum type    = args[5]->Int32Value();
//  void *pixels   = getImageData(args[6]);
//
//  glReadPixels(x, y, width, height, format, type, pixels);
//
//  NanReturnValue(NanUndefined());
//}

GL_METHOD(ReadPixels) {
  GL_BOILERPLATE;

  GLint x        = info[0]->Int32Value();
  GLint y        = info[1]->Int32Value();
  GLsizei width  = info[2]->Int32Value();
  GLsizei height = info[3]->Int32Value();
  GLenum format  = info[4]->Int32Value();
  GLenum type    = info[5]->Int32Value();
  Nan::TypedArrayContents<char> pixels(info[6]);

  glReadPixels(x, y, width, height, format, type, *pixels);
}

GL_METHOD(GetTexParameter) {
  GL_BOILERPLATE;

  GLenum target = info[0]->Int32Value();
  GLenum pname = info[1]->Int32Value();

  GLint param_value=0;
  glGetTexParameteriv(target, pname, &param_value);

  info.GetReturnValue().Set(Nan::New<v8::Integer>(param_value));
}

GL_METHOD(GetActiveAttrib) {
  GL_BOILERPLATE;

  GLuint program = info[0]->Int32Value();
  GLuint index   = info[1]->Int32Value();

  GLint maxLength;
  glGetProgramiv(program, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxLength);

  char* name = new char[maxLength];
  GLsizei length = 0;
  GLenum  type;
  GLsizei size;
  glGetActiveAttrib(program, index, maxLength, &length, &size, &type, name);

  if (length > 0) {
    v8::Local<v8::Object> activeInfo = Nan::New<v8::Object>();
    Nan::Set(activeInfo
      , Nan::New<v8::String>("size").ToLocalChecked()
      , Nan::New<v8::Integer>(size));
    Nan::Set(activeInfo
      , Nan::New<v8::String>("type").ToLocalChecked()
      , Nan::New<v8::Integer>(type));
    Nan::Set(activeInfo
      , Nan::New<v8::String>("name").ToLocalChecked()
      , Nan::New<v8::String>(name).ToLocalChecked());
    info.GetReturnValue().Set(activeInfo);
  } else {
    info.GetReturnValue().SetNull();
  }

  delete[] name;
}

GL_METHOD(GetActiveUniform) {
  GL_BOILERPLATE;

  GLuint program = info[0]->Int32Value();
  GLuint index   = info[1]->Int32Value();

  GLint maxLength;
  glGetProgramiv(program, GL_ACTIVE_UNIFORM_MAX_LENGTH, &maxLength);


  char* name = new char[maxLength];
  GLsizei length = 0;
  GLenum  type;
  GLsizei size;
  glGetActiveUniform(program, index, maxLength, &length, &size, &type, name);

  if (length > 0) {
    v8::Local<v8::Object> activeInfo = Nan::New<v8::Object>();
    Nan::Set(activeInfo
      , Nan::New<v8::String>("size").ToLocalChecked()
      , Nan::New<v8::Integer>(size));
    Nan::Set(activeInfo
      , Nan::New<v8::String>("type").ToLocalChecked()
      , Nan::New<v8::Integer>(type));
    Nan::Set(activeInfo
      , Nan::New<v8::String>("name").ToLocalChecked()
      , Nan::New<v8::String>(name).ToLocalChecked());
    info.GetReturnValue().Set(activeInfo);
  } else {
    info.GetReturnValue().SetNull();
  }

  delete[] name;
}

GL_METHOD(GetAttachedShaders) {
  GL_BOILERPLATE;

  GLuint program = info[0]->Int32Value();

  GLint numAttachedShaders;
  glGetProgramiv(program, GL_ATTACHED_SHADERS, &numAttachedShaders);

  GLuint* shaders = new GLuint[numAttachedShaders];
  GLsizei count;
  glGetAttachedShaders(program, numAttachedShaders, &count, shaders);

  v8::Local<v8::Array> shadersArr = Nan::New<v8::Array>(count);
  for (int i=0; i<count; i++) {
    Nan::Set(shadersArr, i, Nan::New<v8::Integer>((int)shaders[i]));
  }

  info.GetReturnValue().Set(shadersArr);

  delete[] shaders;
}

GL_METHOD(GetParameter) {
  GL_BOILERPLATE;

  GLenum name = info[0]->Int32Value();

  switch(name) {
    case 0x9240 /* UNPACK_FLIP_Y_WEBGL */:
      info.GetReturnValue().Set(
        Nan::New<v8::Boolean>(inst->unpack_flip_y));
    return;

    case 0x9241 /* UNPACK_PREMULTIPLY_ALPHA_WEBGL*/:
      info.GetReturnValue().Set(
        Nan::New<v8::Boolean>(inst->unpack_premultiply_alpha));
    return;

    case 0x9243 /* UNPACK_COLORSPACE_CONVERSION_WEBGL */:
      info.GetReturnValue().Set(
        Nan::New<v8::Integer>(inst->unpack_colorspace_conversion));
    return;

    case GL_BLEND:
    case GL_CULL_FACE:
    case GL_DEPTH_TEST:
    case GL_DEPTH_WRITEMASK:
    case GL_DITHER:
    case GL_POLYGON_OFFSET_FILL:
    case GL_SAMPLE_COVERAGE_INVERT:
    case GL_SCISSOR_TEST:
    case GL_STENCIL_TEST:
    {
      GLboolean params;
      glGetBooleanv(name, &params);

      info.GetReturnValue().Set(Nan::New<v8::Boolean>(params != 0));

      return;
    }

    case GL_DEPTH_CLEAR_VALUE:
    case GL_LINE_WIDTH:
    case GL_POLYGON_OFFSET_FACTOR:
    case GL_POLYGON_OFFSET_UNITS:
    case GL_SAMPLE_COVERAGE_VALUE:
    {
      GLfloat params;
      glGetFloatv(name, &params);

      info.GetReturnValue().Set(Nan::New<v8::Number>(params));

      return;
    }

    case GL_RENDERER:
    case GL_SHADING_LANGUAGE_VERSION:
    case GL_VENDOR:
    case GL_VERSION:
    case GL_EXTENSIONS:
    {
      const char *params = reinterpret_cast<const char*>(glGetString(name));
      if(params) {
        info.GetReturnValue().Set(
          Nan::New<v8::String>(params).ToLocalChecked());
      }
      return;
    }

    case GL_MAX_VIEWPORT_DIMS:
    {
      GLint params[2];
      glGetIntegerv(name, params);

      v8::Local<v8::Array> arr = Nan::New<v8::Array>(2);
      Nan::Set(arr, 0, Nan::New<v8::Integer>(params[0]));
      Nan::Set(arr, 1, Nan::New<v8::Integer>(params[1]));
      info.GetReturnValue().Set(arr);

      return;
    }

    case GL_SCISSOR_BOX:
    case GL_VIEWPORT:
    {
      GLint params[4];
      glGetIntegerv(name, params);

      v8::Local<v8::Array> arr=Nan::New<v8::Array>(4);
      Nan::Set(arr, 0, Nan::New<v8::Integer>(params[0]));
      Nan::Set(arr, 1, Nan::New<v8::Integer>(params[1]));
      Nan::Set(arr, 2, Nan::New<v8::Integer>(params[2]));
      Nan::Set(arr, 3, Nan::New<v8::Integer>(params[3]));
      info.GetReturnValue().Set(arr);

      return;
    }

    case GL_ALIASED_LINE_WIDTH_RANGE:
    case GL_ALIASED_POINT_SIZE_RANGE:
    case GL_DEPTH_RANGE:
    {
      GLfloat params[2];
      glGetFloatv(name, params);

      v8::Local<v8::Array> arr=Nan::New<v8::Array>(2);
      Nan::Set(arr, 0, Nan::New<v8::Number>(params[0]));
      Nan::Set(arr, 1, Nan::New<v8::Number>(params[1]));
      info.GetReturnValue().Set(arr);

      return;
    }

    case GL_BLEND_COLOR:
    case GL_COLOR_CLEAR_VALUE:
    {
      GLfloat params[4];
      glGetFloatv(name, params);

      v8::Local<v8::Array> arr = Nan::New<v8::Array>(4);
      Nan::Set(arr, 0, Nan::New<v8::Number>(params[0]));
      Nan::Set(arr, 1, Nan::New<v8::Number>(params[1]));
      Nan::Set(arr, 2, Nan::New<v8::Number>(params[2]));
      Nan::Set(arr, 3, Nan::New<v8::Number>(params[3]));
      info.GetReturnValue().Set(arr);

      return;
    }

    case GL_COLOR_WRITEMASK:
    {
      GLboolean params[4];
      glGetBooleanv(name, params);

      v8::Local<v8::Array> arr = Nan::New<v8::Array>(4);
      Nan::Set(arr, 0, Nan::New<v8::Boolean>(params[0] == GL_TRUE));
      Nan::Set(arr, 1, Nan::New<v8::Boolean>(params[1] == GL_TRUE));
      Nan::Set(arr, 2, Nan::New<v8::Boolean>(params[2] == GL_TRUE));
      Nan::Set(arr, 3, Nan::New<v8::Boolean>(params[3] == GL_TRUE));
      info.GetReturnValue().Set(arr);

      return;
    }

    default:
    {
      GLint params;
      glGetIntegerv(name, &params);
      info.GetReturnValue().Set(Nan::New<v8::Integer>(params));
      return;
    }
  }
}

GL_METHOD(GetBufferParameter) {
  GL_BOILERPLATE;

  GLenum target = info[0]->Int32Value();
  GLenum pname  = info[1]->Int32Value();

  GLint params;
  glGetBufferParameteriv(target, pname, &params);

  info.GetReturnValue().Set(Nan::New<v8::Integer>(params));
}

GL_METHOD(GetFramebufferAttachmentParameter) {
  GL_BOILERPLATE;

  GLenum target     = info[0]->Int32Value();
  GLenum attachment = info[1]->Int32Value();
  GLenum pname      = info[2]->Int32Value();

  GLint params;
  glGetFramebufferAttachmentParameteriv(target, attachment, pname, &params);

  info.GetReturnValue().Set(Nan::New<v8::Integer>(params));
}

GL_METHOD(GetProgramInfoLog) {
  GL_BOILERPLATE;

  GLuint program = info[0]->Int32Value();

  GLint infoLogLength;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

  char* error = new char[infoLogLength+1];
  glGetProgramInfoLog(program, infoLogLength+1, &infoLogLength, error);

  info.GetReturnValue().Set(
    Nan::New<v8::String>(error).ToLocalChecked());

  delete[] error;
}

GL_METHOD(GetRenderbufferParameter) {
  GL_BOILERPLATE;

  GLenum target = info[0]->Int32Value();
  GLenum pname  = info[1]->Int32Value();

  int value;
  glGetRenderbufferParameteriv(target, pname, &value);

  info.GetReturnValue().Set(Nan::New<v8::Integer>(value));
}

//GL_METHOD(GetUniform) {
//  GL_BOILERPLATE;
//
//  GLuint program = info[0]->Int32Value();
//  GLint location = info[1]->Int32Value();
//  if(location < 0 ) NanReturnValue(NanUndefined());
//
//  float data[16]; // worst case scenario is 16 floats
//
//  glGetUniformfv(program, location, data);
//
//  Local<Array> arr=Nan::New<Array>(16);
//  for(int i=0;i<16;i++)
//    arr->Set(i,JS_FLOAT(data[i]));
//
//  NanReturnValue(arr);
//}

GL_METHOD(GetUniform) {
  GL_BOILERPLATE;

  GLint program  = info[0]->Int32Value();
  GLint location = info[1]->Int32Value();

  float data[4096];
  glGetUniformfv(program, location, data);

  v8::Local<v8::Array> arr = Nan::New<v8::Array>(16);
  for (int i=0; i<16; i++) {
    Nan::Set(arr, i, Nan::New<v8::Number>(data[i]));
  }

  info.GetReturnValue().Set(arr);
}

GL_METHOD(GetVertexAttrib) {
  GL_BOILERPLATE;

  GLint index  = info[0]->Int32Value();
  GLenum pname = info[1]->Int32Value();

  GLint value;

  switch (pname) {
    case GL_VERTEX_ATTRIB_ARRAY_ENABLED:
    case GL_VERTEX_ATTRIB_ARRAY_NORMALIZED:
    {
      glGetVertexAttribiv(index, pname, &value);
      info.GetReturnValue().Set(Nan::New<v8::Boolean>(value != 0));
      return;
    }

    case GL_VERTEX_ATTRIB_ARRAY_SIZE:
    case GL_VERTEX_ATTRIB_ARRAY_STRIDE:
    case GL_VERTEX_ATTRIB_ARRAY_TYPE:
    {
      glGetVertexAttribiv(index, pname, &value);
      info.GetReturnValue().Set(Nan::New<v8::Integer>(value));
      return;
    }

    case GL_VERTEX_ATTRIB_ARRAY_BUFFER_BINDING:
    {
      glGetVertexAttribiv(index, pname, &value);
      info.GetReturnValue().Set(Nan::New<v8::Integer>(value));
      return;
    }

    case GL_CURRENT_VERTEX_ATTRIB:
    {
      float vextex_attribs[4];

      glGetVertexAttribfv(index, pname, vextex_attribs);

      v8::Local<v8::Array> arr=Nan::New<v8::Array>(4);
      Nan::Set(arr, 0,
        Nan::New<v8::Number>(vextex_attribs[0]));
      Nan::Set(arr, 1,
        Nan::New<v8::Number>(vextex_attribs[1]));
      Nan::Set(arr, 2,
        Nan::New<v8::Number>(vextex_attribs[2]));
      Nan::Set(arr, 3,
        Nan::New<v8::Number>(vextex_attribs[3]));
      info.GetReturnValue().Set(arr);

      return;
    }

    default:
      inst->setError(GL_INVALID_ENUM);
  }

  info.GetReturnValue().SetNull();
}

GL_METHOD(GetSupportedExtensions) {
  GL_BOILERPLATE;

  const char *extensions = reinterpret_cast<const char*>(
    glGetString(GL_EXTENSIONS));

  info.GetReturnValue().Set(
    Nan::New<v8::String>(extensions).ToLocalChecked());
}

// TODO GetExtension(name) return the extension name if found, should be an object...
//GL_METHOD(GetExtension) {
//  GL_BOILERPLATE;
//
//  NanAsciiString name(info[0]);
//  char *sname=*name;
//  char *extensions=(char*) glGetString(GL_EXTENSIONS);
//
//  char *ext=strcasestr(extensions, sname);
//
//  if(!ext) NanReturnValue(NanUndefined());
//  NanReturnValue(JS_STR(ext, (int)::strlen(sname)));
//}

GL_METHOD(GetExtension) {
  GL_BOILERPLATE;

  //TODO
}

GL_METHOD(CheckFramebufferStatus) {
  GL_BOILERPLATE;

  GLenum target=info[0]->Int32Value();

  //  NanReturnValue(JS_INT((int)glCheckFramebufferStatus(target)));
  info.GetReturnValue().Set(
      Nan::New<v8::Integer>(
        static_cast<int>(glCheckFramebufferStatus(target))));
}
